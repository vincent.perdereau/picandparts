<?php
class Ced_CsMarketplace_Block_Vebay_Form extends Mage_Core_Block_Template
{

    public function getDatePickerHtml($dateId)
    {
        $form = new Varien_Data_Form(array(
            'id' => $dateId,
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));
        $element = new Varien_Data_Form_Element_Date(
            array(
                'name' => $dateId,
                'label' => $dateId,
                'tabindex' => 1,
                'image' => $this->getSkinUrl('images/calendar.gif'),
                'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                'value' => isset($_REQUEST[$dateId]) ? gmdate('Y-m-d\Th:i:s',strtotime($_REQUEST[$dateId])) : date(
                    Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                    strtotime('next weekday')
                )
            )
        );
        $element->setForm($form);
        $element->setId($dateId);
        return $element->getElementHtml();
    }
}