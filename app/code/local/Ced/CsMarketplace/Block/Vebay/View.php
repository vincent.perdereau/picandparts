<?php
require_once(Mage::getBaseDir().'/lib/Zend/Service/Ebay/Trading.php');
class Ced_CsMarketplace_Block_Vebay_View extends Mage_Core_Block_Template
{
    private $_trading;

    public function __construct() {
        $this->_trading = new Trading();
    }

    public function getCollection($userId) {
        $collection = $this->_trading->getSellerList($userId);
        Mage::getSingleton('core/session')->setData('ebay_api_response',$collection);
        return $collection;
    }
}