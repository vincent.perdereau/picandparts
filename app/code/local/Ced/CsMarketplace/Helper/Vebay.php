<?php
class Ced_CsMarketplace_Helper_Vebay extends Mage_Core_Helper_Abstract {

    private $requestToken;
    private $devID;
    private $appID;
    private $certID;
    private $serverUrl;
    private $compatLevel;
    private $siteID;
    private $verb;
    private $importDir;

    /**	__construct
    Constructor to make a new instance of eBaySession with the details needed to make a call
    Input:	$userRequestToken - the authentication token fir the user making the call
    $developerID - Developer key obtained when registered at http://developer.ebay.com
    $applicationID - Application key obtained when registered at http://developer.ebay.com
    $certificateID - Certificate key obtained when registered at http://developer.ebay.com
    $useTestServer - Boolean, if true then Sandbox server is used, otherwise production server is used
    $compatabilityLevel - API version this is compatable with
    $siteToUseID - the Id of the eBay site to associate the call iwht (0 = US, 2 = Canada, 3 = UK, ...)
    $callName  - The name of the call being made (e.g. 'GeteBayOfficialTime')
    Output:	Response string returned by the server
     */
    public function __construct($userRequestToken, $developerID, $applicationID, $certificateID, $serverUrl,
                                $compatabilityLevel, $siteToUseID, $callName)
    {
        $this->requestToken = $userRequestToken;
        $this->devID = $developerID;
        $this->appID = $applicationID;
        $this->certID = $certificateID;
        $this->compatLevel = $compatabilityLevel;
        $this->siteID = $siteToUseID;
        $this->verb = $callName;
        $this->serverUrl = $serverUrl;
        $this->importDir = Mage::getBaseDir('media') . DS . 'import' . DS;
    }


    /**	sendHttpRequest
    Sends a HTTP request to the server for this session
    Input:	$requestBody
    Output:	The HTTP Response as a String
     */
    public function sendHttpRequest($requestBody)
    {
        //build eBay headers using variables passed via constructor
        $headers = $this->buildEbayHeaders();

        //initialise a CURL session
        $connection = curl_init();
        //set the server we are using (could be Sandbox or Production server)
        curl_setopt($connection, CURLOPT_URL, $this->serverUrl);

        //stop CURL from verifying the peer's certificate
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

        //set the headers using the array of headers
        curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);

        //set method as POST
        curl_setopt($connection, CURLOPT_POST, 1);

        //set the XML body of the request
        curl_setopt($connection, CURLOPT_POSTFIELDS, $requestBody);

        //set it to return the transfer as a string from curl_exec
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);

        //Send the Request
        $response = curl_exec($connection);

        //close the connection
        curl_close($connection);

        //return the response
        return $response;
    }



    /**	buildEbayHeaders
    Generates an array of string to be used as the headers for the HTTP request to eBay
    Output:	String Array of Headers applicable for this call
     */
    private function buildEbayHeaders()
    {
        $headers = array (
            //Regulates versioning of the XML interface for the API
            'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->compatLevel,

            //set the keys
            'X-EBAY-API-DEV-NAME: ' . $this->devID,
            'X-EBAY-API-APP-NAME: ' . $this->appID,
            'X-EBAY-API-CERT-NAME: ' . $this->certID,

            //the name of the call we are requesting
            'X-EBAY-API-CALL-NAME: ' . $this->verb,

            //SiteID must also be set in the Request's XML
            //SiteID = 0  (US) - UK = 3, Canada = 2, Australia = 15, ....
            //SiteID Indicates the eBay site to associate the call with
            'X-EBAY-API-SITEID: ' . $this->siteID,
        );

        return $headers;
    }

    public function importItem($item) {
        $item_id = $item['ItemID'];
        $i=0;
        $path = array();
        foreach($item['galleryURL'] as $p){
            $content=@file_get_contents($p);
            $save = $item_id . $i. '.jpg';
            $path[$i]=$this->importDir.''.$save;
            @file_put_contents($path[$i],$content);
            $i++;
        }

        $categoriesPath = explode(":", $item['PrimaryCategory']);
        if (count($categoriesPath) > 2) {
            $parentCategory = Mage::getModel('catalog/category')->loadByAttribute('name', $categoriesPath[count($categoriesPath) - 2]);
        }

        if (!$parentCategory) {
            $parentCategory = Mage::getModel('catalog/category')->loadByAttribute('name', 'Parts & Accessories');
        }

        $category = Mage::getResourceModel('catalog/category_collection')->addAttributeToSelect('id')
            ->addAttributeToFilter('parent_id',$parentCategory->getId())
            ->addAttributeToFilter('name', array(
                'like' => '%'.$categoriesPath[count($categoriesPath) - 1].'%'
            ));


        return $this->additem($item, $item_id, '20', $item['Title'], $path, $category->getFirstItem()->getEntityId());
    }

    public function additem($item, $id,$price,$name,$path,$category_id,$name_value){
        $exist = false;
        $pro  = Mage::getModel('catalog/product')->loadByAttribute('sku', $id);
        if (!$pro) {
            $pro  = Mage::getModel('catalog/product');
        } else {
            $exist = true;
            $pro=Mage::getModel('catalog/product')->load($pro->getId());
        }

        foreach($name_value as $nv){
            $pro->setData($nv[0],$nv[1]);
        }

        //on clean les vieilles images
        if ($exist) {
            $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            $items = $mediaApi->items($pro->getId());
            foreach ($items as $item)
                $mediaApi->remove($pro->getId(), $item['file']);
        }

        $count=0;
        foreach ($path as $p) :
            if ($count == 0) :
                $pro->addImageToMediaGallery($p, array('thumbnail','image','small_image'), false);
            else :
                $pro->addImageToMediaGallery($p,null, false, false );
            endif;
            $count++;
        endforeach;

        $pro->setType('Simple Product');
        $pro->setWebsiteIds(array(1));
        $pro->setSku($id);
        if ($item['SKU']) {
            $pro->setData('mpn', $item['SKU']);
        }
        $pro->setPrice(isset($item['StartPrice']) ? $item['StartPrice'] : $price);
        $pro->setCategoryIds(array($category_id));
        $pro->setAttributeSetId(Mage::getModel('catalog/product')->getResource()->getEntityType()->getDefaultAttributeSetId());
        $pro->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $pro->setTypeId('simple');
        $pro->setName($name);
        $pro->setDescription('<p></p>');
        $pro->setShortDescription('<p></p>');
        $pro->setstatus(1);
        $pro->setTaxClassId(0);
        $pro->setWeight(0);
        $pro->setStockData(array(
            'is_in_stock' => 1,
            'qty' => 1
        ));
        $pro->setCreatedAt(strtotime('now'));

        //$manufacturerInfos = Mage::helper('partsfinder/edmundsapi')->edmunds_api_req('4T1BK1EB6DU056165');

        $compatibilities = $this->getCompatibility($id);

        $helper = Mage::helper('partsfinder');

        if (isset($compatibilities['makes'])) {
            foreach($compatibilities['full'] as $full) {
                $helper->setOrAddOptionAttribute($pro, 'compatibilities', $full);
            }
        }

        if (isset($compatibilities['makes'])) {
            foreach($compatibilities['makes'] as $make) {
                $pro = $helper->setOrAddOptionAttribute($pro, 'marque', $make);
            }
        }

        if (isset($compatibilities['years'])) {
            foreach($compatibilities['years'] as $year) {
                $pro = $helper->setOrAddOptionAttribute($pro, 'annee_fabrication', $year);
            }
        }

        if (isset($compatibilities['models'])) {
            foreach($compatibilities['models'] as $model) {
                $pro = $helper->setOrAddOptionAttribute($pro, 'model', $model);
            }
        }


        $pro->save();
        return $pro;
    }


    protected function getCompatibility($ItemID) {

        $SafeItemID = urlencode($ItemID);
        $endpoint = 'http://open.api.ebay.com/shopping';  // URL to call
        $responseEncoding = 'NV';                         // Type of response we want back - need to enable JSON extension

// Construct the call
        $apicall = "$endpoint?callname=GetSingleItem&version=517&siteid=0"
            . "&appid=magekey-SellerLi-PRD-93a0284bf-5c2ee7bb"
            . "&ItemID=$SafeItemID"
            . "&responseencoding=$responseEncoding"
            . "&IncludeSelector=Compatibility";

// Load the call and capture the document returned by eBay API
        parse_str(file_get_contents($apicall), $resp);    // populate the $resp array

        $xml = simplexml_load_file("http://open.api.ebay.com/shopping?callname=GetSingleItem&version=517&responseencoding=XML&siteid=0&appid=magekey-SellerLi-PRD-93a0284bf-5c2ee7bb&ItemID=".$SafeItemID."&IncludeSelector=Compatibility");
        $ack = strtolower( ( string ) $xml->Ack );
        $makes = array();
        $models = array();
        $years = array();
        $full = array();

        if( $ack == 'success' ) {
            foreach( $xml->Item as $item ) {
                foreach($item->ItemCompatibilityList->children() as $compatibility) {
                    foreach($compatibility->children() as $child) {

                        if ((string)$child->Name == 'Make') {
                            //if (!in_array((string)$child->Value, $makes)) {
                                $make = (string)$child->Value;
                                $makes[] = $make;
                            //}
                        } elseif ((string)$child->Name == 'Year') {
                            //if (!in_array((string)$child->Value, $years)) {
                                $year = (string)$child->Value;
                                $years[] = $year;
                            //}
                        } elseif ((string)$child->Name == 'Model') {
                            //if (!in_array((string)$child->Value, $models)) {
                                $model = (string)$child->Value;
                                $models[] = $model;
                           // }
                        }

                        if ($make && $model && $year) {
                            if (!in_array($make . ' # ' . $model . ' # ' . $year, $full)) {
                                $full[] = $make . ' # ' . $model . ' # ' . $year;
                                $make = $model = $year = null;
                            }
                        }
                    }
                }
            }
        }

        return array('full' => $full, 'makes' => $makes, 'years' => $years, 'models' => $models);
    }

}
?>
