<?php
class Magekey_Insurance_Model_Observer extends Varien_Event_Observer
{

    public function addCustomOption($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $options = $product->getOptions();
        if ($options){
            foreach ($options as $option) {
                if ($option['title'] == 'Options' && $option['type'] == 'checkbox' && !$option['is_delete']) {
                    //we've already added the option
                    return;
                }
            }
        }
        $option = array(
            'title' => 'Options',
            'type' => 'checkbox',
            'is_require' => 0,
            'sort_order' => 0,
            'values' => array(
                array(
                    'title' => 'Insurance',
                    'price' =>4,
                    'price_type' => 'percent',
                    'sku' => '',
                    'sort_order' => '1'
                ))
        );

        $product->setCanSaveCustomOptions(true);
        $product->getOptionInstance()->addOption($option);
        $product->setHasOptions(true);
    }
}
?>