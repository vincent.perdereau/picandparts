<?php
class Magekey_Partsfinder_Model_Observer extends Varien_Event_Observer
{

    public function newVendorProductCreation($observer)
    {
        //COMPATIBILITIES
        $product = $observer->getEvent()->getProduct();
        $productData=Mage::app()->getRequest()->getParams();
        $this->setCompatibilities($product, $productData);

        //SKU
        $sku = preg_replace('#[^0-9a-z]+#i', '-', $product->getName());
        $sku = substr(strtolower($sku),0,64);
        $product->setSku($sku);
        $product->getResource()->save($product);
    }

    public function editVendorProductCreation($observer) {
        $product = $observer->getEvent()->getProduct();
        $productData=Mage::app()->getRequest()->getParams();
        $compatibilities = Mage::getModel('partsfinder/compatibility')->getCollection()->addFieldToFilter('product_id', $product->getId())->load();

        foreach($compatibilities as $compatibility) {
            $compatibility->delete();
        }
        $this->setCompatibilities($product, $productData);
    }

    protected function setCompatibilities($product, $productData) {
        $helper = Mage::helper('partsfinder');
        $i = 0;

        if (isset($productData['marque'])) {
            foreach($productData['marque'] as $make) {
                $compatibility = $make;
                $helper->setOrAddOptionAttribute($product, 'marque', $make);
                if (isset($productData['model'])) {
                    if (count($productData['model']) > $i) {
                        $compatibility .= ' - ' . $productData['model'][$i];
                        $helper->setOrAddOptionAttribute($product, 'model', $productData['model'][$i]);
                    }
                }
                if (isset($productData['annee_fabrication'])) {
                    if (count($productData['annee_fabrication']) > $i) {
                        foreach($productData['annee_fabrication'][$i] as $year) {
                            $helper->setOrAddOptionAttribute($product,'compatibilities', $compatibility . ' - ' . $year);
                            $helper->setOrAddOptionAttribute($product, 'annee_fabrication', $year);
                        }
                    } else {
                        $helper->setOrAddOptionAttribute($product,'compatibilities', $compatibility);
                    }
                } else {
                    $helper->setOrAddOptionAttribute($product,'compatibilities', $compatibility);
                }

                $i++;
            }
        }
    }
}
?>