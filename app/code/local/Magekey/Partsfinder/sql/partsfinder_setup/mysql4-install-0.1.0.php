<?php
$installer = $this;
$installer->startSetup();
$installer->run("
 DROP TABLE IF EXISTS {$installer->getTable('partsfinder_compatibility')};
CREATE TABLE IF NOT EXISTS `{$installer->getTable('partsfinder_compatibility')}` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `make` varchar(50) COLLATE utf8_unicode_ci,
  `model` varchar(50) COLLATE utf8_unicode_ci,
  `years` varchar(255) COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	");


$installer->endSetup();