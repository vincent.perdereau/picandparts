<?php

class Magekey_Partsfinder_Helper_Edmundsapi extends Mage_Core_Helper_Abstract
{

    protected $edmunds_api_key = 'pzv32q3r5ydn9ddv2g7gbjap';

    public function edmunds_api_req($vin, $postData = array(''))
    {

        $url = 'https://api.edmunds.com/api/vehicle/v2/vins/' . $vin . '?fmt=json&api_key=' . $this->edmunds_api_key;

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        $responseData = json_decode($response, TRUE);
        curl_close($ch);
        sleep(2);
        return $responseData;
    }

    public function getMakes($postData = array('')) {
        $url = 'https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=' . $this->edmunds_api_key;

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        $responseData = json_decode($response, TRUE);
        curl_close($ch);
        sleep(2);
        Mage::getSingleton('core/session')->setData('makes', $responseData['makes']);
        return $responseData['makes'];
    }

    public function getModels($make, $postData = array('')) {

        if ($datas = Mage::getSingleton('core/session')->getData('makes')) {
            foreach($datas as $data) {
                if ($data['name'] == $make) {
                    Mage::getSingleton('core/session')->setData('models', $data['models']);
                    return $data['models'];
                }
            }
        }

        $url = 'http://api.edmunds.com/api/vehicle/v2/'.$make.'?fmt=json&api_key=' . $this->edmunds_api_key;

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        $responseData = json_decode($response, TRUE);
        curl_close($ch);
        sleep(2);
        if ($responseData['status'] != 'METHOD_NOT_ALLOWED') {
            return $responseData['models'];
        }
        return false;
    }

    public function getYears($make, $model, $postData = array('')) {

        if ($datas = Mage::getSingleton('core/session')->getData('models')) {
            foreach($datas as $data) {
                if ($data['name'] == $model) {
                    return $data['years'];
                }
            }
        }

        $url = 'http://api.edmunds.com/api/vehicle/v2/'.$make.'/'.$model.'/years?fmt=json&api_key=' . $this->edmunds_api_key;

        $url = str_replace ( ' ', '%20',$url);
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        $responseData = json_decode($response, TRUE);
        curl_close($ch);
        sleep(3);
        if ($responseData['status'] != 'METHOD_NOT_ALLOWED') {
            return $responseData['years'];
        }
        return false;
    }
}