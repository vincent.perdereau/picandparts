<?php
class Magekey_Partsfinder_Block_Brands extends Mage_Core_Block_Template {

    public function getBrands() {
        return Mage::helper('partsfinder/vehicules')->getMakes();
    }
}