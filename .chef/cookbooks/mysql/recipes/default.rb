# Installation des paquets
package "mysql-server"

# Mise en place du mot de passe root
execute "mysqladmin" do
  command "mysqladmin -u root password #{node[:mysql][:pass_root]}"
  action :run
  only_if "mysql -u root -e 'show databases;'"
end

# Configuration
template "/etc/mysql/conf.d/magento.cnf" do
  source "mysql.conf.erb"
end

# On efface les fichiers de logs
file "/var/lib/mysql/ib_logfile0" do
    action :delete
end
file "/var/lib/mysql/ib_logfile1" do
    owner "root"
    action :delete
end

# Prise en compte des changements
service "mysql" do
  action :restart
end
