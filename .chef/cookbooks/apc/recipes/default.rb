# Installation
package 'php-apc'

# Configuration
template '/etc/php5/apache2/conf.d/20-apc.ini' do
  source    'apc.conf.erb'
end
