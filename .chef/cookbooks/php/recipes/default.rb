# Installation
package 'php5'
package 'php5-gd'
package 'php5-mcrypt'
package 'php5-curl'
package 'php5-xsl'

# Configuration
template '/etc/php5/apache2/conf.d/30-magento.ini' do
  source    'php.conf.erb'
end

