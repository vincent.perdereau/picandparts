<?php


require_once(Mage::getBaseDir().'/lib/Zend/Service/Ebay/EbaySession.php');
class Trading {

//$id = '271334389513';

//$responseDoc->getElementsByTagName('SKU')->item(0)->nodeValue;
//SiteID must also be set in the Request's XML
//SiteID = 0  (US) - UK = 3, Canada = 2, Australia = 15, ....
//SiteID Indicates the eBay site to associate the call with
private $siteID = 0;
//the call being made:
    private $verb = 'GetItem';

// SANDBOX
    private $userToken = 'AgAAAA**AQAAAA**aAAAAA**fpd5Vw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GgAZiKpgSdj6x9nY+seQ**S8MDAA**AAMAAA**dGXgD8OGEXYKrB9sPy5+qxBJkS7dOL7Eb0liMiJrxNyClXuyA4VS0me1YKioKuPPmiup4z9jjuoTxI42RwH2uaiKWyfRKWYjYR0l9ZZjVrKOvF8TxgfnXusVwpE0zUS8Sr1tNuVS9y9iHAbdWxN86LCF3t1mvoAHAr1B4oNaonglAK/EXIIMRvD+eks4vjHc24BJSQG7kR0kIeWrKdtsWPOaSaR1wzkAzxUbZ+9YNaVkYiE3/PnUmzE6kUWljTXf9QrKirT9XZ0OIZnrcMaYOdp3CJttbiqL5Ge4hVY/otZMOdE2L6j7o2+uBEMw+ZgpC/AzLyYjfP0D0MHIiRotEPlXDxxO8HHO0nOFNzEH4ovdpRbOu5KpbiXQuHd1fynZ+75NKARYQ3FmSUaZmKGg5MhHhAmKNW6Zo8d47xQPKrNHC8HatrAEOdEsNx2vtCT4AGppbvD1sXFrajjEkQvieXxUjXjkAsVKFqlpiD4lTWUuGFCROrelc15gmDllkpIICs3KWZ5jyNK+Ynjs+oKCnedzOByoxrIMyrFQJdA4iKv3j2cMLUgGmqvO0MV+GG6DUJ7onmDU1Lb3wcQ3XXMbAgwVAPngIJOqtaD42oRpRs+w0z2fVw2u6zwYt3qZUU7VFGNrmBQcA9lqJxHCAPCvR8DdFgJTyYwFqOEnCndP1IcmA14qRZ0t0G2T4ncSxjmrcuxmePh+tcIxH+LqLLXP38Sq6DW+7L3wXlkD6HMdb17FrbP0A/MRknw14SHdhEEM';
  //  private $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';

// PRODUCTION
    private $userToken_prod = 'AgAAAA**AQAAAA**aAAAAA**nrV6Vw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ABkICpC5eFoQ6dj6x9nY+seQ**xzsDAA**AAMAAA**qywqnA+LYatXS/BB2At5pB9OIMgF7q1g79aus5OvaDQB0fVwGveJfBDDe9RK4A+GaILTCFBTNoq7XDP/JExnd92UX6hm1End1P8cEMPPo9W2XFQiYfSWuMlgssdioXHq5DtbtFBOOczgLDwhX1gEMI0IvSTqv7d8NvIxtva8G26BJL8pdIpLDuFq6Y3djPmjow9qYKhfl582+nE8CJla9J4z2RDIy7hLBe5a1iKI2dD4FJn8Tz8hexV5AqHW9PjVal+xI2aH/w7NkiRQbtUiKQZ5cmljphQCTAGDaV+az9bYHTwosvkhG6Lb8B0Pv+9z7csWoxfLBjgkiYqeXLieHxkygzzqCfgvc/nNuA5uz5xTzm/NzbGm+CE+Fd6ZSngTgOCvauImdqv7OeJInjJ2XNzX5sUBaLWOE5z7onC6WOpAxOAj75OcH7ngNQABwN8wpNSXUH4cbdH3rGhmVA057NYrGiBnca3uxopUX2wd5BIf+sWz4g5fUC55KL5qFttyJSGeVinwtVKIPzxHEsZSXyidrxlHxstht6kFtddXEuiqfUz2kMgoeLqJreqENigm9fK7EhHnIVKcYAhAClIftR3RXO8bBD70igOJSDtLt16j1q9lD9lul5oTceWFx7SME7pJQxcPByWODVjg1G7xmpyCxni7hrJp39kLK0+8I4s61g4QFc+fqbrlkfg1WackYtySttIJSUtzBNg+wRgnop4ayZjwXnUDL/h1s5cCEkM8626OULIRCqQnYtgHwBAK';
    private $devId = '27ddd256-7783-46cd-aed5-980a6760627a';
    private $appId = 'magekey-SellerLi-PRD-93a0284bf-5c2ee7bb';
    private $certId = 'PRD-3a0284bf6ea9-bbb1-4ec2-9ff0-be6a';
    private $serverUrl = 'https://api.ebay.com/ws/api.dll';
    private $session;

///Build the request Xml string

public function __construct() {

}

    public function getItem($itemId) {
        $session = new eBaySession($this->userToken_prod, $this->devId, $this->appId, $this->certId, $this->serverUrl, 963, 0, 'GetItem');

        $requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
        $requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
       $requestXmlBody .= '<OutputSelector>SKU</OutputSelector>';
        $requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$this->userToken_prod</eBayAuthToken></RequesterCredentials>";;
        $requestXmlBody .= "<ItemID>$itemId</ItemID>";
        $requestXmlBody .= '</GetItemRequest>';
        $responseDoc = $this->execute($requestXmlBody);
       // return $responseDoc->saveXml();
        return $responseDoc->getElementsByTagName('SKU')->item(0)->nodeValue;
    }

public function execute($requestXmlBody, EbaySession $session) {

//send the request and get response
    $responseXml = $session->sendHttpRequest($requestXmlBody);
    if(stristr($responseXml, 'HTTP 404') || $responseXml == '')
        die('<P>Error sending request');

    //Xml string is parsed and creates a DOM Document object
    $responseDoc = new DomDocument();
    $responseDoc->loadXML($responseXml);


    //get any error nodes
    $errors = $responseDoc->getElementsByTagName('Errors');

    //if there are error nodes
    if($errors->length > 0)
    {
        return false;
    } else //no errors
    {
        return $responseDoc;
    }

}


    public function getSellerList($userId) {
        if (!isset($_REQUEST['DateFrom']) || !$_REQUEST['DateTo']) {
            $start = $end = gmdate('Y-m-d\Th:i:s');
        } else {
            $start = date('Y-m-d\Th:i:s',strtotime($_REQUEST['DateFrom']));
            $end = date('Y-m-d\Th:i:s',strtotime($_REQUEST['DateTo']));
        }

        $session = new eBaySession($this->userToken_prod, $this->devId, $this->appId, $this->certId, $this->serverUrl, 825, 0, 'GetSellerList');

        $requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
        $requestXmlBody .= '<GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
        $requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$this->userToken_prod</eBayAuthToken></RequesterCredentials>";
        $requestXmlBody .= "<UserID>$userId</UserID>";
        $requestXmlBody .= '<Pagination>';
/*        $requestXmlBody .= '<EntriesPerPage></EntriesPerPage>';*/
        $requestXmlBody .= '<PageNumber>1</PageNumber>';
        $requestXmlBody .= '</Pagination>';
        $requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
        $requestXmlBody .= '<GranularityLevel>Fine</GranularityLevel>';
        $requestXmlBody .= '<StartTimeFrom>'.$start.'</StartTimeFrom>';
        $requestXmlBody .= '<StartTimeTo>'.$end.'</StartTimeTo>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.ItemID</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.SKU</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.StartPrice</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.PictureDetails</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.ListingDetails</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.Quantity</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.Variations</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.Title</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.PrimaryCategory.CategoryID</OutputSelector>';
        $requestXmlBody .= '<OutputSelector>ItemArray.Item.PrimaryCategory.CategoryName</OutputSelector>';
        $requestXmlBody .= '</GetSellerListRequest>';
        $responseDoc = $this->execute($requestXmlBody, $session);

        if (!$responseDoc) {
            return false;
        }

        $list = array();

        foreach ($responseDoc->getElementsByTagName('ItemArray') as $items) {
            foreach ($items->getElementsByTagName('Item') as $item ) {
                $nodes = array();
                foreach($item->childNodes as $child) {
                    if ($child->nodeName == 'PictureDetails') {
                        $nodes['galleryURL'] = array();
                        foreach ($child->childNodes as $subnode) {
                            if ($subnode->tagName == 'PictureURL') {
                                array_push($nodes['galleryURL'], $subnode->nodeValue);
                            }
                        }
                    } else if ($child->nodeName == 'ListingDetails') {
                        $nodes['viewItemURL'] = $child->getElementsByTagName('ViewItemURL')->item(0)->nodeValue;
                    } else if ($child->nodeName == 'Variations') {
                        $nodes['EAN'] = $child->getElementsByTagName('EAN')->item(0)->nodeValue;
                    } else {
                        $value = $child->nodeValue;
                        $nodes[$child->nodeName] = $value;
                    }
                }
                $list[$nodes['ItemID']] = $nodes;
            }
        }
        return $list;
    }

}